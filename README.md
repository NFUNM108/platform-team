# DataFountain

## 目录
* [DataFountain介绍](#DataFountain介绍)
* [平台画布](#平台画布)
* [核心交互](#核心交互)
* [平台理论及分析成果](#平台理论及分析成果)
  * [三大功能](#三大功能)
    * [吸引](#吸引)
    * [促进](#促进)
    * [匹配](#匹配)
* [衡量指标](#衡量指标)
* [治理](#治理)
  * [平台治理](#平台治理)
  * [赛事治理](#赛事治理)
  * [成果分析](#成果分析)
* [平台建议](#平台建议)
* [大数据理论](#大数据理论)
* [大数据分析](#大数据分析)
* [大数据建议](#大数据建议) 
* [总结](#总结)
  * [论点一](#论点一)
  * [论点二](#论点二)
  * [论点三](#论点三)


## DataFountain介绍

- DataFountain是一个专业的数据竞赛平台，主要承接业界各种数据竞赛，将数据分析等技术问题发布至平台，吸引数据科学家以及相关的爱好者来解决问题。




## 平台画布

![平台画布](https://images.gitee.com/uploads/images/2019/0627/174345_fd214a7c_1648220.png "13d90078cc5e276c18ab57dd6ee1642.png")

## 核心交互

- 参与者

||B端用户|C端用户|
|--|--|--|
|说明|大多是拥有庞大数据集的公司或企业|拥有数据分析能力或想学习大数据知识的用户
|角色|如：BAT、IBM以兴业银行等科技巨头、高校院所、蚂蚁金服、唯品会|如：数据科学家、统计学家、数据工作者、学生团队、科研所、企业从事人员

- 价值单元：一次被数据科学家或大学生团队成功报名并上传作品的数据竞赛。



- 过滤器

1. 赛题任务设定，规定接口方法，设置学术壁垒。

![比赛任务](https://images.gitee.com/uploads/images/2019/0623/225838_4da4d04b_1648220.png "比赛任务.png")

2. 社群的加入，设定了邀请码，这样可以维护社群的良好环境，保证社群知识分享的质量。

![验证码](https://images.gitee.com/uploads/images/2019/0623/225913_3f72d52b_1648220.png "邀请码.png")

- 主要价值：C端数据科学家、大学生团队或集团团体通过报名参加DataFountain的一个数据竞赛后，上传分析作品为开放数据并组织数据比赛的B端企业科技研究所解决数据问题，为公司或机构创造效益。

## 平台理论及分析成果

### 三大功能

#### 吸引
-  :smile: 理论观点一：吸引参与者对于DataFountain来说不是难事，但想保持用户黏性还需要有**反馈回路**，并且尽可能**扩大网络效应**。
- 成果分析：

1.根据[easycounter](https://www.easycounter.com/report/datafountain.cn)网站的可数数据显示，DataFountain的每日综合浏览量有26.7K，与竞品阿里云天池相比，DataFountain的受众面狭窄，99.8%都集中在中国。

|阿里云天池|DataFountain|
|--|--|
|![阿里云](https://images.gitee.com/uploads/images/2019/0626/233252_da817cd5_1648220.png "阿里云.png")|![dataFountain](https://images.gitee.com/uploads/images/2019/0626/233315_cda41d4b_1648220.png "datafountain.png")|

我认为在其吸引用户方面还有待提高。

2.反馈回路方面，对于竞赛只是简单的报名→提交作品→查看排名，C端的数据分析者无法从成功报名参加一次比赛的价值单元基础上得到反馈。对于业界高手，你能明显的看到自己的排名位置，可对于小白，DataFountain没有更多促进交互的活动。

#### 促进
-  :smile: 理论观点二：所谓促进就需要设定一些价值可以被创造，促进过程需要减少使用的障碍。在促进更多C端数据分析师参加比赛中，DataFountain减低了报名门槛。但对于B端主办方要举办一场比赛，门槛却如平台中**Sittercity**案例里一样对注册的保姆有一套严格的规定。

- 成果分析

1.报名参加比赛步骤简单：C端用户可以**无缝进入**比赛，只需要点击报名参赛即可以参加比赛。

![image.png](https://images.gitee.com/uploads/images/2019/0626/233108_c493aad8_1648220.png)

2.社区进入设有一定门槛：经过亲测，用户想进入社区需要有一定的理由，并提供你能贡献的方面以及需要解答的问题，如果合理，平台将会给予你验证码。

![image.png](https://images.gitee.com/uploads/images/2019/0626/233107_6d6d93e0_1648220.png)

3.组织比赛的要求严格：主办方要举办比赛需要先填写DataFountain的举办赛事表单，接受审核。并且赛题也需要严格的设计。赛题任务、赛题规则都需要在有一定的数据背景下才能得以设立的。

![咨询表单](https://images.gitee.com/uploads/images/2019/0628/150119_508d53ad_1648220.png "1561704898(1).png")

#### 匹配
-  :smile: 理论观点三：DataFountain想要精准地匹配用户，就需要让C端拥有不同数据分析能力的用户（业界大咖、计算机专业的大学生、公司团队）能够正确定位自己，找到适合自己能力的竞赛去报名和参加。

- 成果分析：如果一味设置高阶比赛，可能会让新人望而止步。所以DataFountain为竞赛做一个从初级入门、中级入门到高级入门的梯度分类。根据C端用户的能力程度开设比赛。
> DataFountain 透漏了一组数字：在 2016 年的 CCF 大赛，有 55% 的参赛者是在校学生。

但这些比赛需要C端用户自己去观察，DataFountain没有辅助推荐。


## 衡量指标
-  :smile: 理论观点四：在2017年1月搜狐发布的一则新闻中DataFountain注册用户数达到9K +，但当用户数量达到一个临界值时，我们还要保证核心交互可以创造价值。

- 成果分析：2019定位的转变可以看出DataFountain励志先让参赛者培养成数据科学家。在我看来其还是处在一个**发展阶段**。从如今行业的发展前景来看，数据科学家还是稀有人才，所以DataFountain在培养人才这一方面还处在一个有待提高的阶段。DataFountain需要去发现究竟有多少人去实践其线上的实验室。以及仍然需要去保持竞赛的活跃度。

1.保持赛事活跃度：结合平台**流动性**知识，利用一个月内发出奖金与一个月设定的全部奖金金额的比值来衡量C端是否有活跃参加竞赛。

2.转化用户上：通过竞赛基础上转化为线上实验室学生与竞赛成员的比值来衡量其培训的受欢迎程度，以及用户转化率。


## 治理

-  :smile: 理论观点5

### 平台治理
- DataFountain的创始团队是**中科院计算所**，中科院计算机所是**中国科学院**下的**研究单位**。并且中国科学院的主管部门是国家的国务院。

### 赛事治理 
- 根据对赛事的了解，每一个比赛的主办单位都是不同的，但都不是靠私营企业来主办的。
- 例子：如 “运动想象无训练数据集” 比赛，其主办单位是**国家自然科学基金委员会信息科学部**、中国电子学会、清华大学医学院，而协办单位才是博睿康科技（常州）股份有限公司、蓝色传感（北京）科技有限公司等有限公司。
- 治理机构分析：国家自然科学基金委员会信息科学部是是国家自然科学基金委员会的**学术性管理机构**，而国家自然科学基金委员会则是国务院下建立的**事业单位**。

### 成果分析
- 我认为这是属于**国家监管**，在陈娟的专访中说到，DataFountain正在构建一个众包项目平台，承接一些企业和政府的项目。结合《大数据时代》数据创新的知识 —— **开放数据**，国家自然科学基金委员会信息科学部拥有高速网络及信息安全、高性能计算等数据资源以及软件技术，将其开放可以通过比赛解决数据问题，创造价值。


## 平台建议



### 建议一 —— 平台一端用户吸引另一端用户
- 建议方案：DataFountain的创始团队是中科院计算所，同时从2017年用户分布中我们可以获知，目前有**1000+所高校，51所国家级科研院**参加比赛，在我看来这是天池以及DataCastle无法比拟的资源。因为受中国科学院的管制，DataFountain拥有雄厚的学术背景。并且通过[中科院计算所](http://www.ict.cas.cn/gjjl/xshd/)的官网了解到每年都有国际交流活动。如果各高校导师可以积极带领其下的团队，同时鼓励呼吁国际交流团队参加，那其C端用户的范围将会扩大。再加上比赛周期长久便可以提高C端用户黏性。

### 建议二 —— 添加新价值单元
- 平台革新：2018年12月——DataFountain创始人专访
> “18年之前，公司的定位是数据科学领域专业的数据竞赛平台，但是在未来的发展规划上，我们计划把这个平台升级成为数据科学家学习及工作平台，这个是公司战略上的变化，也是19年战略重点。”—— 来源：中国大数据产业观察数据观 
- 思考：平台想从数据竞赛平台转变为能让科学家们相互交流学习的平台，必定要**从竞争转变为竞合与分享**。其重点单边C端用户能够分享自己的知识，对此DataFountain需要打造的是一个数据科学人才交流互助的社区。利用浏览量以及有用的虚拟货币反馈充当过滤器来对社区帖子进行排名。但如何促进C端用户不断创造价值，这是平台需要相对应给予C端用户的价值。

![反馈](https://images.gitee.com/uploads/images/2019/0626/233107_a09f13e3_1648220.png)

- 建议方案：DataFountain拥有高质量的社区，首先进入此社区需要验证，再者社区有大咖入驻。使得社区的知识更具专业性，这样的优势是阿里天池等国内其他竞品没有的。因此可以利用反馈的过滤器来优化创新，创造新的价值单元。而且目前DataFountain只为得奖的C端用户给予工作机会，过于局限。—— 参考追波网的案例以及Kaggle平台，平台可以开设一个界面功能“job"给专门要招聘数据分析师的公司。此时平台可以为第三方推荐排名较靠前的数据科学家，新的价值单元即为**数据驱动型公司通过平台成功找到数据分析师，数据分析师成功入职该数据驱动公司。**

### 建议三 —— 盈利模式，究竟向谁收费 
- 思考： DataCastle CEO 张琳艳说过："竞赛是小众低频的行为，并且竞赛对于举办方的要求也高。" 因此单靠竞赛的交易费来获取盈利，那平台得到的微乎其微。
- 建议方案：结合建议二“引入第三方招聘公司”的策略，平台可以向他们收取**准入费**。

### 建议四 —— 推荐系统
- 思考：根据平台理论以及分析的成果，在**吸引**方面没有促进长尾效应头部的用户进一步交互，以及在**匹配**方面没有通过C端用户能力来推荐赛事。

- 建议方案：从静态信息如身份，年龄，以及动态信息如参加比赛的类型难度以及爱好，将其**量化**为数据，如电商平台一样给C端用户进行个性化推荐。

## 大数据理论

### 价值之开放数据
- 提取数据价值最好的方法就是允许私营部门和社会大众访问。DataFountain平台就是鼓励业界各方企业开放自己的数据，使其可以进行互相的交流及分析。

## 大数据分析

- 在DataFounctain转向数据科学家平台后，更多的是需要开放的资源提供学习。
> 未来企业的开放程度将直接决定数据科学家的培养速度。
-  **B端企业不想开放数据**：从DataFountain 2016~2018三年来的竞赛场数观察中可以看出目前举办比赛的B端企业仍然较少，而且呈现增长缓慢的趋势。由此可以看出目前大多数企业都无法改变传统观念，对数据进行共享。

|2016|2017|2018|
|--|--|--|
|11|20|21|

结合DataCastle的CEO张琳艳对B端企业的分析可以总结如下原因：

a.**无法利用数据**：拥有大量的数据但不知道如何去使用，如何利用数据创造价值。

b.**竞赛平台给予的价值不高**：一次竞赛的举办B端主办方不仅需要支付奖金还需要支付交易费，但发现其最后从C端用户中得到的分析结果并没有为企业带来很大的效益。

## 大数据建议

### 建议a
- 要让企业知道数据在于使用而不在于占有，那就要让其看到数据分析背后蕴含的潜在价值。
- **上演价值创造**——与其他公司进行长期合作并创造客观价值。2018年京东与DataFountain共同设立了**CCF大数据与计算智能大赛**。在我看来DataFountain可以利用前期的合作基础，继续举办比赛，并且通过赛事挖掘专业的C端团队进行长期合作。以此造势，如果像京东这样如此具有影响力的电商平台都可以从数据竞赛中得到价值，那将会吸引更多B端企业的加入。

### 建议b
- DataFountain的许多比赛都是能得到当地政府的支持。如2019年3月举办的**文化传承—汉字书法多场景识别**比赛，其主办单位有**福州市人民政府**，从大数据的理论中知道提取政府数据价值最好的办法就是允许社会大众的访问。那DataFountain可以与各地区政府合作，当地政府可以像英国一样利用相关规定鼓励信息公开，那么B端企业、想要往数据驱动型转型的公司也会逐渐开放自己的数据。


## 总结

### 论点一

- 从缺乏企业的开放数据我们知道DataFountain缺少B端的企业用户，同时平台吸引C端用户的力度不足，从这两点可以得出DataFountain在利用核心交互促进双边参与者进行价值创造上还有待提高。

### 平台革新
- 自2019建立培养数据科学家的目标，DataFountain建立线上培训——“人工智能教学实验室”。

a. 在百度指数中“大数据”以及“数据分析”关键词的同比和环比指数都有所增长，关键词“数据分析”环比增长幅度较大，说明数据分析如今发展势头很足，会有更加多的参与者进入市场。

![](https://images.gitee.com/uploads/images/2019/0626/233108_f85236d2_1648220.png)

b.再者，结合两个关键词的用户画像分析，要想让更多的企业往数据驱动型转型则必须培养新一代数据科学家。我认为处于20~29岁的用户正是急需培养的对象。这一段年龄层的C端用户对数据分析的认识日益增加，其自身有一定的知识水平，同时还比较年轻，通过一定的锻炼，他们会是新一代数据科学家的最好人选。

![](https://images.gitee.com/uploads/images/2019/0626/233108_611feb65_1648220.png)

综上两条理由所属，DataFountain的转型发展前景是光明的。

### 面临问题
- 根据百度指数所提供的TGI指数来看，目前在关注大数据、数据分析领域的C端用户中，对于家电数码以及旅游出行的兴趣较高，而对于软件应用、教育培训、医疗健康的兴趣则稍低。

![image.png](https://images.gitee.com/uploads/images/2019/0626/233108_cc157300_1648220.png)

### 建议c
- 平台开设培训实验室进行案例分析时，其案例来源可以更多来自于与旅行平台，电商平台合作。这样的改变有利于增强C端用户的学习兴趣，即可以促进B端企业开放数据又可以培养C端的数据科学家。

### 论点二

- 大数据论点中B端企业家不愿开放数据的原因是认为通过比赛得到的价值不大。结合平台理论中DataFountain的匹配工作做得不足，导致参加比赛的C端用户技术水平参差不齐，其分析的数据成果并没有为B端企业贡献很大的价值。要改变B端企业的观念需要DataFountain提高其策展能力，在匹配双方用户时制定更加有效的机制，提高比赛的整体质量。

### 论点三

- 结合开放数据需要有领头羊的带动作用，以及DataFountain自身的良好治理。创办比赛如果有各地区政府机构下部门的允许与推动，比如教育局的推动，会让比赛的网络效应急速扩大。通过政府机构鼓励企业开放数据，并且政府本身开放统计数据，充当领头羊的角色，可以让更多的B端企业看到DataFountain平台的重要价值。s/)